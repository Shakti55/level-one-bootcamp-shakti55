#include <stdio.h>
#include <math.h>


int inputp()
{
    float p;
    scanf("%f",&p);
    return p;
}

int inputq()
{
    float q;
    scanf("%f",&q);
    return q;
}


void print (float dist)
{
    printf("Distance between the points = %f\n",dist);
}


float calc(float p1, float q1,float p2,float q2)
{
     return (sqrt(( (p2-p1)*(p2-p1) )+( (q2-q1)*(q2-q1) )));
}




int main() 
{
	float p1, q1, p2, q2;
	
	printf("Input p1: ");
	p1=inputp();
	
	printf("Input q1: ");
	q1=inputq();
	
	printf("Input p2: ");
	p2=inputp();
	
	printf("Input q2: ");
	q2=inputq();
	
    float dist = calc(p1,q1,p2,q2);
	print(dist);
	return 0;
}