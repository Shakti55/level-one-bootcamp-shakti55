//Write a program to find the sum of n different numbers using 4 functions
#include<stdio.h>

int main()
{
    int n, sum = 0, c, array[100];

    printf("Enter the number of integers: ");
    scanf("%d", &n);

    printf("\nEnter the %d integers", n);

    for(c = 0; c < n; c++)
    {
        scanf("%d", &array[c]);
        sum += array[c];   
    }

    printf("\nSum of the numbers = %d", sum);
    return 0;
}