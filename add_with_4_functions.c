//Write a program to add two user input numbers using 4 functions.
#include<stdio.h>

int inputp()
{
  int x;
  printf("Enter the first number");
  scanf("%d",&x);
  return x;
}

int inputq()
{
  int x;
  printf("Enter the second number");
  scanf("%d", &x);
  return x;
}

int add(int p, int q)
{
   return(p+q);
}
void print(int sum)
{
   printf("Sum=%d\n",sum);
}

int main()
{
  int p=inputp();
  int q=inputq();
  int sum=add(p,q);
  print(sum);
  
  return 0;
}
