//WAP to find the distance between two points using structures and 4 functions.
#include <stdio.h>
#include <math.h>

struct Point 
{ 
   float p, q; 
}z1,z2; 


float inputp()
{
    float p;
    printf("Enter the co-ordinate of p: ");
    scanf("%f",&p);
    return p;
}

float inputq()
{
    float q;
    printf("Enter the co-ordinate of q: ");
    scanf("%f",&q);
    return q;
}


void print (float dist)
{
    printf("Distance between the points  = %f\n",dist);
}


float calc(float p1, float q1, float p2, float q2)
{
     return (sqrt(( (p2-p1)*(p2-p1) )+( (q2-q1)*(q2-q1) )));
}




int main() 
{
	float p1, q1, p2, q2;
	
	printf("Input p1: ");
	z1.p=inputp();
	
	printf("Input q1: ");
	z1.q=inputq();
	
	printf("Input p2: ");
	z2.p=inputp();
	
	printf("Input q2: ");
	z2.q=inputq();
	
    float dist = calc(z1.p,z1.q,z2.p,z2.q);
	print(dist);
	return 0;
}