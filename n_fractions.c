//WAP to find the sum of n fractions.
#include <stdio.h>


int input()
{
    int p;
    scanf("%d",&p);
    return p;
}

int gcdcal(int p, int q)
{
    int gcd=1;
    for(int i=1; i <= p && i <= q; ++i)
    {
        if(p%i==0 && q%i==0)
            gcd = i;
    }
    return gcd;
}

int main()
{

int numo,deno,p,q,gcd;

int n,numo1,numo2,deno1,deno2;
printf("Enter the number of fractions to add:" );
n=input();

printf("Enter the numerator and denominator of the 1st fraction :" );
numo1=input();
deno1=input();

printf("\n" );

for(int i=0;i<n-1;i++)
{
    printf("Enter the numerator and denominator of the fraction:");
    numo2=input();
    deno2=input();
    p=(numo1*deno2)+(deno1*numo2);
    q=deno1*deno2; 


    gcd = gcdcal(p,q);
    numo1=p/gcd;
    deno1=q/gcd;
}


printf("\nThe sum of the fractions is %d/%d" ,p/gcd,q/gcd);
printf("\n");
return 0;
}